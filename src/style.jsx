import { keyframes } from 'styled-components';

export const loadingAnimation = keyframes`
    0% {
        stroke-dashoffset: 440;
    }

    100% {
      stroke-dashoffset: 440;
    }

    50% {
      stroke-dashoffset: 0;
    }

    50.1% {
      stroke-dashoffset: 880;
    }
`;

export const loadingRotate = keyframes`
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
`;

export const breathing = keyframes`
  0% {
    -webkit-transform: scale(0.9);
    -ms-transform: scale(0.9);
    transform: scale(0.9);
  }

  25% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  60% {
    -webkit-transform: scale(0.9);
    -ms-transform: scale(0.9);
    transform: scale(0.9);
  }

  100% {
    -webkit-transform: scale(0.9);
    -ms-transform: scale(0.9);
    transform: scale(0.9);
  }
`;

export const slide = keyframes`
  100% { right: 10px; }
`;

export const zoomIn = keyframes`
  0% {
    transform: scale(.3, 1.3);
  }
  100% {
    transform: scale(1, 1);
  }
`;

export const slideUp = keyframes`
  0% {
    transform: translateY(250px);
  }
  100% {
    transform: translateY(0);
  }
`;
