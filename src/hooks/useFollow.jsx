import { useState } from 'react';
import AppApi from '../utils/axios';

const useFollow = () => {

    const [follows, setFollows] = useState(null);

    
    const postFollow = async (values) => {
        const res = await AppApi('post', '/follow', values);
        return res;
    };

    const getUserFollows = async (user_id) => {
        const data = await AppApi('get', `/user-follows?id=${user_id}`);

        if (!data || !data.length) {
            return;
        }

        setFollows(data);
    }

    return {
        postFollow,
        getUserFollows,
        follows
    }
}

export default useFollow;