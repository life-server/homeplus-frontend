import React from 'react';
import Payment from '../../../components/DashboardPayment';
import { Box } from '@mui/system';
import Typography from '@mui/material/Typography';
import { shallow } from 'enzyme';

describe('../../../components/DashboardPayment', () => {
  it('should render Payment by default', () => {
    const wrapper = shallow(<Payment  BSB_NUM="0195672" ACC_NUM="0195672" />);
    expect(wrapper.find(Box));
    expect(wrapper.find(Typography));
  });
});