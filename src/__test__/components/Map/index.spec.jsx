import React from 'react';
import Map from '../../../components/Map';
import { GoogleMap, useLoadScript, Marker, InfoWindow, MarkerClusterer } from '@react-google-maps/api';
import { shallow } from 'enzyme';

describe('../../../components/CustomButton', () => {
  it('should render Map by default', () => {
    const wrapper = shallow(<Map tasks={[1,2,3,4,5,6,7]}  address="testing" distance="testing" />);
    expect(wrapper.find(GoogleMap));
    expect(wrapper.find(useLoadScript));
    expect(wrapper.find(Marker));
    expect(wrapper.find(InfoWindow));
    expect(wrapper.find(MarkerClusterer));
  });
});