import React from 'react';
import TaskerDetail from '../../../components/TaskerDetail';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import { shallow } from 'enzyme';

describe('../../../components/TaskerDetail', () => {
  it('should render TaskerDetail by default', () => {
    const wrapper = shallow(<TaskerDetail  tasker={{}} taskerReviews={[]} />);
    expect(wrapper.find(Card));
    expect(wrapper.find(Box));
  });
});