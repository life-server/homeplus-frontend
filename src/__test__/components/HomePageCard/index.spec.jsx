import React from 'react';
import HomePageCard from '../../../components/HomepageCard';
import { shallow } from 'enzyme';

describe('../../../components/HomepageCard', () => {
  it('should render HomepageCard by default', () => {
    const wrapper = shallow(<HomePageCard  type="testing" />);
    expect(wrapper.containsAnyMatchingElements([
      <h2>TESTING</h2>
    ]));
  });
});