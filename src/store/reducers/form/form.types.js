const TaskFormActionTypes = {
  UPDATE_FIELD: 'UPDATE_FIELD',
  BACK_TO_INIT: 'BACK_TO_INIT',
};

export default TaskFormActionTypes;
