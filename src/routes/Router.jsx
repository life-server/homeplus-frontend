import React, { lazy } from 'react';
import { Route as RouterPage, Routes, Navigate } from 'react-router-dom';
import ProtectedRouterPage from './components';

const HomePage = lazy(() => import('../pages/HomePage'));
const ErrorPage = lazy(() => import('../pages/ErrorPage'));
const PostTaskPage = lazy(() => import('../pages/PostTaskPage'));
const DashboardPage = lazy(() => import('../pages/DashboardPage'));
const TaskerDetailPage = lazy(() => import('../pages/TaskerDetailPage'));
const BrowsePage = lazy(() => import('../pages/BrowsePage'));
const AccountVerifyPage = lazy(() => import('../pages/AccountVerifyPage'));
const ResetPasswordPage = lazy(() => import('../pages/RestPasswordPage'));
const MapBrowsePage = lazy(() => import('../pages/MapBrowsePage'));

const Route = () => (
  <Routes>
    <RouterPage path="/" element={<Navigate replace to="/home" />} />
    <RouterPage path="/home" element={<HomePage />} />
    <RouterPage path="/post-task" element={<PostTaskPage />} />
    <RouterPage path="/tasker-detail/:tasker_id/:tasker_name" element={<TaskerDetailPage />} />
    <RouterPage path="/browse-tasks" element={<MapBrowsePage />} />
    <RouterPage path="/account-verify/:token" element={<AccountVerifyPage />} />
    <RouterPage path="/password-reset/:token" element={<ResetPasswordPage />} />
    <RouterPage path="/browse-tasks/:task_id/:title" element={<MapBrowsePage />} />
    <RouterPage path="/browse-tasks-origin" element={<BrowsePage />} />
    <RouterPage path="/browse-tasks-origin/:task_id/:title" element={<BrowsePage />} />

    <RouterPage element={<ProtectedRouterPage />}>
      <RouterPage path="/dashboard" element={<DashboardPage />} />
    </RouterPage>
    <RouterPage path="*" element={<ErrorPage />} />
  </Routes>
);

export default Route;
