import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Box from '@mui/material/Box';
import TaskerDetail from '../../components/TaskerDetail';
import useTasker from '../../hooks/useTasker';
import useReview from '../../hooks/useReview';
import LoadingPage from '../../components/LoadingPage';

export default function TaskerDetailPage() {
  const { requestTaskerById, taskerById} = useTasker();
  const { 
    requestTaskerReviews, 
    reviews
  } = useReview();
  const { tasker_id } = useParams();
  const [ tasker, setTasker] = useState(null);
  const [ taskerReviews, setReviews ] = useState(null);
  const [ taskerId, setTaskerId] = useState(null);
  const [ isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (tasker_id && taskerId !== tasker_id) {
      requestTaskerById(tasker_id);
      requestTaskerReviews(tasker_id);
      setTaskerId(tasker_id);
    }
    if (taskerById && taskerById !== tasker) {
      setTasker(taskerById);
    }
    if (reviews && reviews !== taskerReviews ) {
      setReviews(reviews);
      setIsLoading(false);
    }
    
  }, [requestTaskerById, tasker, taskerById, tasker_id, taskerId, requestTaskerReviews, reviews, taskerReviews])

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', marginTop: '50px' }}>
      { isLoading ? <LoadingPage/> : <TaskerDetail tasker={ tasker } taskerReviews={ taskerReviews }/>}
    </Box>
  );
}