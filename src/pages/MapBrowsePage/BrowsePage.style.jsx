import styled from 'styled-components';
import { breathing, slide } from '../../style';

export const Browse = styled.div`
  height: 94vh;
  overflow-x: hidden;
  h3 {
    margin-top: 10px;
    margin-bottom: 7px;
  }
  @media (max-width: 768px) {
    h3 {
      font-size: 18px;
      margin-top: 5px;
      margin-bottom: 3px;
    }
  }
`;

export const MapBox = styled.div`
  width: 100%;
  height: 94vh;
  .searchSet {
    .searchBar {
      width: 600px;
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.23), 0 6px 6px rgba(0, 0, 0, 0.25);
    }
    display: flex;
    align-items: center;
    gap: 30px;
    position: absolute;
    top: 7%;
    left: 50%;
    transform: translateX(-50%);
  }
  .filterBall {
    width: 55px;
    height: 55px;
    background-color: #fff;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: 3px;
    border-radius: 50%;
    -webkit-animation: ${breathing} 3s ease-out infinite normal;
    animation: ${breathing} 3s ease-out infinite normal;
    cursor: pointer;
    .filterLine1,
    .filterLine2,
    .filterLine3 {
      height: 2.5px;
      width: 32px;
      background: #146EB4;
      margin-bottom: 4px;
      border-radius: 5px;
      &:after{
        content: '';
        position: absolute;
        height: 8px;
        width: 8px;
        background: #146EB4;
        margin-top: -2.3px;
        border-radius: 50%;
        transition: all 0.5s ease-in-out;
      }
    }

    .filterLine1{
      &:after{
        transform: translatex(6px);
      }
    }

    .filterLine2{
      &:after{
        transform: translatex(18px);
      }
    }

    .filterLine3{
      &:after{
        transform: translatex(2px);
      }
    }

    
  }

  .onFilter{
    .filterLine1{
      &:after{
        transform: translatex(18px);
      }
    }

    .filterLine2{
      &:after{
        transform: translatex(6px);
      }
    }

    .filterLine3{
      &:after{
        transform: translatex(14px);
      }
    }
  }
  @media (max-width: 768px) {
    height: 94vh;
    .searchSet {
      .searchBar {
        width: 250px;
    }
  }
`;

export const ToolBar = styled.div`
  position: absolute;
  top: 17%;
  right: 5%;
  display: flex;
  width: 550px;
  justify-content: start;
  background-color: #fff;
  border-radius: 10px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  .tools {
    width: 100%;
    position: relative;
    padding: 6%;
    padding-top: 0.7rem;
    gap: 2px;
    .filterHeader {
      display: flex;
      align-items: center;
      justify-content: space-between;
    }
    .toolButtonsSet {
      display: flex;
      align-items: center;
    }
    .sliderBox {
      margin-top: 35px;
      width: 100%;
    }
  }
  .MuiSelect-select {
    color: #444;
  }
  @media (max-width: 768px) {
    left: 50%;
    transform: translateX(-50%);
    width: 350px;
    .tools {
      .filterHeader {
        margin-left: 8px;
      }
      .sliderBox {
        margin-top: 25px;
        width: 100%;
      }
    }
  }
`;

export const Display = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  right: -900px;
  top: 0;
  height: 93vh;
  max-width: 800px;
  -webkit-animation: ${slide} 0.5s forwards;
  -webkit-animation-delay: 0.5s;
  animation: ${slide} 0.5s forwards;
  animation-delay: 0.5s;
  z-index: 99;
  .details {
    width: 780px;
    height: 100%;
    padding-top: 0;
    position: relative;
    .backToMap {
      position: absolute;
      width: 45px;
      height: 45px;
      background-color: #afafaf;
      transition: all 0.3s ease-in-out;
      top: 40%;
      left: -4%;
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
      display: flex;
      align-items: center;
      justify-content: center;
      border-radius: 50%;
      :hover {
        background-color: #555;
        cursor: pointer;
      }
    }
  }

  @media (max-width: 768px) {
    height: 94vh;
    width: 100%;
    .details {
      width: 100%;
      padding-left: 5%;
      height: 94vh;
      .backToMap {
        display: none;
      }
    }
  }
`;

export const Contents = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 93vh;
  padding-top: 30px;
  justify-content: center;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  gap: 20px;
  width: 350px;
  overflow: scroll;
  background-color: #fff;
  .loadingAndError {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const LocationBox = styled.div`
  display: flex;
  align-items: center;
  gap: 15px;
  margin-bottom: 30px;
  .MuiTextField-root {
    margin-top: 0.8rem;
  }

  .locationBox-buttons {
    margin-top: 0.8rem;
    padding-left: 8px;
  }
`;
