export const sortTypeSwitch = (type) => {
  const now = Date.now();
  let ms;
  switch (type) {
    case 'a':
      return (a) => new Date(a.date) > now;
    case 'threeDays':
      ms = new Date().getTime() + 86400000 * 3;
      let after3Days = new Date(ms);
      return (a) => new Date(a.date) > Date.now() && new Date(a.date) < after3Days;
    case 'aWeek':
      ms = new Date().getTime() + 86400000 * 7;
      let aWeek = new Date(ms);

      return (a) => new Date(a.date) > Date.now() && new Date(a.date) < aWeek;
    case 'aMonth':
      ms = new Date().getTime() + 86400000 * 30;
      let aMonth = new Date(ms);
      return (a) => new Date(a.date) > Date.now() && new Date(a.date) < aMonth;
    default:
      return;
  }
};

export const sortTypeItems = [
  {
    label: 'Date: Most Recent',
    value: 'recent',
  },
  {
    label: 'Date: Futrue',
    value: 'future',
  },
  {
    label: 'Budget: High - Low',
    value: 'high budget',
  },
  {
    label: 'Budget: Low - High',
    value: 'low budget',
  },
];

export const typeItems = [
  {
    label: 'All',
    value: 'e',
  },
  {
    label: 'Cleaning only',
    value: 'cleaning',
  },
  {
    label: 'Moving only',
    value: 'removal',
  },
  {
    label: 'Repair only',
    value: 'handyperson',
  },
];
