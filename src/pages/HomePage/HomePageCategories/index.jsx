import React from 'react';
import { Box, CardContainer } from './HomePageCategories.style';
import Card from '../../../components/CategoriesCard';
import Cleaning from '../../../assets/cleaning.png';
import Removalists from '../../../assets/sofa.png';
import Handyperson from '../../../assets/mechanic.png';
import Delivery from '../../../assets/delivery-truck.png';
import Gardening from '../../../assets/gardening.png';
import Electricians from '../../../assets/electrician.png';
import Assembly from '../../../assets/industrial-robot.png';
import Painter from '../../../assets/swatches.png';

const HomePageCategories = () => (
  <Box>
    <h2>What do you need done?</h2>
    <CardContainer>
      <Card src={Cleaning} title="Cleaning" />
      <Card src={Removalists} title="Removalists" />
      <Card src={Handyperson} title="Handyperson" />
      <Card src={Delivery} title="Delivery" />
      <Card src={Gardening} title="Gardening" />
      <Card src={Electricians} title="Electricians" />
      <Card src={Assembly} title="Assembly" />
      <Card src={Painter} title="Painter" />
    </CardContainer>
  </Box>
);

export default HomePageCategories;
