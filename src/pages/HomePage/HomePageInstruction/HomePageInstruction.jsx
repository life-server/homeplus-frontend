import React from 'react';
import { Box, Container } from './HomePageInstruction.style';
import Card from '../../../components/HomepageCard/HomepageCard';
import Rating from '../../../assets/rate.png';
import Credit from '../../../assets/credit-card.png';
import Insurance from '../../../assets/insurance.png';

const HomePageTaskType = () => (
  <Box>
    <h2>Feel safe and secure on HomePlus</h2>
    <Container>
      <Card src={Credit} title="Secure payments">
        Only release payment when the task is completed to your satisfaction
      </Card>
      <Card src={Rating} title="Trusted ratings and reviews">
        Pick the right person for the task based on real ratings and reviews from other users
      </Card>
      <Card src={Insurance} title="Insurance for peace of mind">
        We provide liability insurance for Taskers performing most task activities
      </Card>
      <br />
    </Container>
  </Box>
);

export default HomePageTaskType;
