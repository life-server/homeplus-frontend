import styled from 'styled-components';

export const StepCard = styled.div`
  width: 230px;
  height: 150px;
  justify-content: center;
  @media (max-width: 768px) {
    margin: 10px 0;
  }
`;
export const StepStyle = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  margin: 0 0 30px;
`;
export const Logo = styled.img`
  margin: 5px;
  width: 120px;
  height: 120px;
`;
export const StepNum = styled.div`
  font-size: 70px;
  color: #b5b5b5;
  font-weight: 900;
  margin: 0 0 0 10px;
`;
export const StepDescription = styled.div`
  font-size: 20px;
  font-weight: 500;
  color: #444;
  display: flex;
  justify-content: center;
  margin-top: 10px;
`;
