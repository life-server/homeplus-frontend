import styled from 'styled-components';
// import { zoomIn } from '../../../style';

export const Box = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 20px 0;
  padding: 80px 0;
  background: #f2f2f2;
`;

export const Container = styled.div`
  margin: 20px 0px;
  justify-content: center;
  h2 {
    display: flex;
    justify-content: center;
  }
  @media (max-width: 768px) {
    align-items: center;
  }
`;
export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 50px 0 50px;
  justify-content: space-evenly;
  gap: 80px;

  > div {
    &:nth-child(1) {
      animation: ${({ scrollPosition }) =>
        scrollPosition > 350 ? `zoomIn .4s cubic-bezier(.17,.67,.83,.67)` : 'none'};
      animation-delay: -.2s;
      transform-origin: center bottom:
    }
    &:nth-child(2) {
      animation: ${({ scrollPosition }) =>
        scrollPosition > 350 ? `zoomIn .7s cubic-bezier(.17,.67,.83,.67)` : 'none'};
      animation-delay: -.2s;
      transform-origin: center bottom:
    }

    &:nth-child(3) {
      animation: ${({ scrollPosition }) => (scrollPosition > 350 ? `zoomIn 1s cubic-bezier(.17,.67,.83,.67)` : 'none')};
      animation-delay: -.2s;
      transform-origin: center bottom:
    }
  }

  @media (max-width: 840px) {
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: 100px;
  }

  @keyframes zoomIn {
    0% {
      opacity: 0;
      transform: scale3d(0.75, 0.75, 1);
    }
    60% {
      opacity: 0;
      transform: scale3d(0.75, 0.75, 1);
    }
    100% {
      opacity: 1;
      transform: scale3d(1, 1, 1);
    }
  }
`;
