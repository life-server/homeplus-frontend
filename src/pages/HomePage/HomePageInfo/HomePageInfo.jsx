import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import CustomButton from '../../../components/CustomButton';
import { Box, Container, PostTaskStyle } from './HomePageInfo.style';
import { askLogin } from '../../../store/reducers/user/user.actions';

const HomePageInfo = () => {
  const currentUser = useSelector((state) => state.currentUser);
  const dispatch = useDispatch();

  return (
    <Box>
      <PostTaskStyle>
        <h1>
          Connect with experts to get the job <br />
          done on HomePlus
        </h1>
        <p>It’s amazing to what you can’t do yourself</p>
        <Container>
          <Link to="/post-task">
            <CustomButton color="primary" variant="contained" size="large">
              Post your task for free
            </CustomButton>
          </Link>
          <Link to="/dashboard">
            <CustomButton
              color="secondary"
              variant="contained"
              size="large"
              onClick={currentUser.loggedIn ? null : () => dispatch(askLogin())}
            >
              Become a Tasker
            </CustomButton>
          </Link>
        </Container>
      </PostTaskStyle>
    </Box>
  );
};

export default HomePageInfo;
