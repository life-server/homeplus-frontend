export const sortTypeSwitch = (type) => {
  switch (type) {
    case 'recent':
      return (a, b) => new Date(a.date) - new Date(b.date);
    case 'future':
      return (a, b) => new Date(b.date) - new Date(a.date);
    case 'high budget':
      return (a, b) => b.budget - a.budget;
    case 'low budget':
      return (a, b) => a.budget - b.budget;
    default:
      return;
  }
};

export const sortTypeItems = [
  {
    label: 'Date: Most Recent',
    value: 'recent',
  },
  {
    label: 'Date: Futrue',
    value: 'future',
  },
  {
    label: 'Budget: High - Low',
    value: 'high budget',
  },
  {
    label: 'Budget: Low - High',
    value: 'low budget',
  },
];

export const typeItems = [
  {
    label: 'All',
    value: 'e',
  },
  {
    label: 'Cleaning only',
    value: 'cleaning',
  },
  {
    label: 'Moving only',
    value: 'removal',
  },
  {
    label: 'Repair only',
    value: 'handyperson',
  },
];
