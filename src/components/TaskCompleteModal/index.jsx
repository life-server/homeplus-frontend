import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Modal from '@mui/material/Modal';
import Rating from '@mui/material/Rating';
import Box from '@mui/material/Box';
import StarIcon from '@mui/icons-material/Star';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import Avatar from '@mui/material/Avatar';
import CustomButton from '../CustomButton';
import stringAvatar from '../../utils/avatar.util';
import { TaskCompleteBox, RatingSection, Tasker, Form } from './TaskCompleteModal.style.jsx';
import useReview from '../../hooks/useReview';

const labels = {
  0.5: 'Useless',
  1: 'Useless+',
  1.5: 'Poor',
  2: 'Poor+',
  2.5: 'Ok',
  3: 'Ok+',
  3.5: 'Good',
  4: 'Good+',
  4.5: 'Excellent',
  5: 'Excellent+',
};

const TaskCompleteModal = ({ setShowModal, showModal, task }) => {
  const { createReview } = useReview();
  const initValues = {
    taskEntity: null,
    rating: 3,
    review: '',
  };
  const [reviewInfo, setReviewInfo] = useState({ ...initValues });
  const [taskEntity, setTaskEntity] = useState(null);
  const [hover, setHover] = useState(-1);

  const handleSubmit = async (e) => {
    e.preventDefault();
    await createReview(reviewInfo);
    setShowModal(false);
  };

  useEffect(() => {
    if (task && taskEntity !== task) {
      setTaskEntity(task);
      setReviewInfo({ ...reviewInfo, taskEntity: task });
    }
  }, [task, taskEntity, reviewInfo]);

  const handleClose = () => setShowModal(false);

  return (
    <>
      {showModal && task ? (
        <Modal open={showModal} onClose={handleClose}>
          <TaskCompleteBox>
            <h3>Task Review</h3>
            <Tasker>
              <div className="avatar">
                <Avatar {...stringAvatar(task.taskerEntity.userEntity.name)} />
                <span>{task.taskerEntity.userEntity.name}</span>
              </div>
            </Tasker>
            <Form onSubmit={handleSubmit}>
              <RatingSection>
                <Rating
                  name="hover-feedback"
                  value={reviewInfo.rating}
                  precision={0.5}
                  size="large"
                  onChange={(event, newValue) => {
                    setReviewInfo({ ...reviewInfo, rating: newValue });
                  }}
                  onChangeActive={(event, newHover) => {
                    setHover(newHover);
                  }}
                  emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
                />
                {reviewInfo.rating !== null && (
                  <Box sx={{ ml: 2 }}>{labels[hover !== -1 ? hover : reviewInfo.rating]}</Box>
                )}
              </RatingSection>
              <FormControl sx={{ m: 1 }}>
                <TextField
                  id="review"
                  onChange={(e) => setReviewInfo({ ...reviewInfo, [e.target.name]: e.target.value })}
                  value={reviewInfo.review}
                  name="review"
                  label="Task review"
                  multiline
                  rows={3}
                  fullWidth
                />
              </FormControl>

              <CustomButton variant="contained" type="submit">
                send
              </CustomButton>
            </Form>
          </TaskCompleteBox>
        </Modal>
      ) : null}
    </>
  );
};

TaskCompleteModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
  task: PropTypes.object,
};

export default TaskCompleteModal;
