import styled from 'styled-components';
import Close from '@mui/icons-material/Close';

export const TaskCompleteBox = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  height: 400px;
  display: flex;
  flex-direction: column;
  align-item: center;
  border-radius: 10px;
  box-shadow: 24;
  background-color: #fff;
  font-family: 'Roboto', sans-serif;
  h3 {
    text-align: center;
    font-weight: 400;
  }
`;

export const RatingSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const Tasker = styled.div`
  margin-top: 1rem;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  span {
    margin-top: 1rem;
  }
  .avatar {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;

export const Form = styled.form`
  margin: 0 auto;
  width: 90%;
  padding: 1.5rem;
  display: flex;
  flex-direction: column;
  position: relative;
  button {
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
`;

export const BankInfo = styled.div`
  display: flex;
  z-index: 10;
`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 1.8;
  color: #141414;
  p {
    margin-bottom: 1rem;
  }
`;

export const CloseModalButton = styled(Close)`
  cursor: pointer;
  position: absolute;
  top: 20px;
  right: 20px;
  width: 32px;
  height: 32px;
  padding: 0;
  z-index: 10;
`;

export const Text = styled.div`
  color: #444;
  align-items: center;
  font-size: 17px;
  display: flex;
`;

export const UsernameText = styled.div`
  color: #444;
  align-items: center;
  font-size: 20px;
  display: flex;
  font-weight: 900;
`;

export const MenuLinkAvatar = styled.img`
  margin: 10px;
  max-width: 55px;
  width: 50px;
  height: 50px;
  object-fit: cover;
  border-radius: 50%;
`;

RatingSection.displayName = 'RatingSection';
BankInfo.displayName = 'BankInfo';
ModalContent.displayName = 'ModalContent';
CloseModalButton.displayName = 'CloseModalButton';
Text.displayName = 'Text';
UsernameText.displayName = 'UsernameText';
Tasker.displayName = 'Tasker';
UsernameText.displayName = 'UsernameText';
MenuLinkAvatar.displayName = 'MenuLinkAvatar';
