import { React } from 'react';

import Stack from '@mui/material/Stack';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import TextField from '@mui/material/TextField';

const CustomDatePicker = ({ ...props }) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Stack spacing={3} sx={{ maxWidth: 600 }}>
        <DatePicker views={['day', 'month', 'year']} renderInput={(params) => <TextField {...params} />} {...props} />
      </Stack>
    </LocalizationProvider>
  );
};

export default CustomDatePicker;
