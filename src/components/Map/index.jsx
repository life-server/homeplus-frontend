import React, { useCallback, useRef, useState, useEffect, useMemo } from 'react';
import { GoogleMap, useLoadScript, Marker, InfoWindow, MarkerClusterer } from '@react-google-maps/api';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import Avatar from '@mui/material/Avatar';
import { TaskInfoWindow, clusterStyle } from './TaskInfoWindow.style';

import { mapStyle } from './Map.style';

import CustomButton from '../CustomButton';
import stringAvatar from '../../utils/avatar.util';
import useGeocode from '../../hooks/useGeocode';
const libraries = ['places'];
const mapContainerStyle = {
  width: '100%',
  height: '100%',
  boxShadow: '0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23)',
};
const center = {
  lat: -33.86882,
  lng: 151.20929,
};

const options = {
  styles: mapStyle,
  disableDefaultUI: true,
  zoomControl: true,
};

const zoom = (distance) => {
  let zoomLevel = 10;
  if (distance === 25) {
    return zoomLevel;
  } else {
    const equatorLength = 6378140;
    const widthInPixels = 1200 / distance;
    let metersPerPixel = equatorLength / 256;
    zoomLevel = 1;
    while (metersPerPixel * widthInPixels > 2000) {
      metersPerPixel /= 2;
      ++zoomLevel;
    }
    return zoomLevel;
  }
};

const Map = ({ tasks, address, distance }) => {
  const [markers, setMarkers] = useState([]);
  const [centerAddress, setAddress] = useState('');
  const { getLatLng, latLng } = useGeocode();
  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
    libraries,
    language: 'en',
  });

  const icon = useCallback((isFollowing) => {
    if (isFollowing) return '/paper-plane-following.png';
    return '/paper-plane.png';
  }, []);

  useEffect(() => {
    if (tasks !== markers && isLoaded) {
      setMarkers(tasks);
    }
    if (!tasks.length && !!markers.length) {
      setMarkers([]);
    }
    if (centerAddress !== address && address !== '') {
      getLatLng(address);
      setAddress(address);
    }
  }, [isLoaded, tasks, markers, centerAddress, getLatLng, address]);

  const [selectedMarker, setSelectedMarker] = useState(null);
  const date = useMemo(() => {
    if (selectedMarker) {
      return <p>{selectedMarker.date.split('T')[0]}</p>;
    }
  }, [selectedMarker]);
  const mapRef = useRef();
  const onMapLoad = useCallback((map) => {
    mapRef.current = map;
  }, []);

  if (loadError) return 'Error loading maps';
  if (!isLoaded) return 'Loading Maps';
  return (
    <GoogleMap
      mapContainerStyle={mapContainerStyle}
      zoom={zoom(distance)}
      center={latLng ? latLng : center}
      options={options}
      onLoad={onMapLoad}
    >
      {!!markers.length && (
        <MarkerClusterer averageCenter enableRetinaIcons styles={clusterStyle}>
          {(clusterer) =>
            markers?.map((marker) => (
              <Marker
                key={marker.id}
                position={{ lat: marker.lat, lng: marker.lng }}
                icon={{
                  url: icon(marker.following),
                  scaledSize: new window.google.maps.Size(30, 30),
                }}
                clusterer={clusterer}
                onClick={() => {
                  setSelectedMarker(marker);
                }}
              />
            ))
          }
        </MarkerClusterer>
      )}
      {selectedMarker ? (
        <InfoWindow
          position={{ lat: selectedMarker.lat, lng: selectedMarker.lng }}
          onCloseClick={() => {
            setSelectedMarker(null);
          }}
        >
          <TaskInfoWindow>
            <div>
              <div className="userTitle">
                <h2>{_.upperFirst(selectedMarker.title)}</h2>
                <div className="user">
                  <Avatar {...stringAvatar(selectedMarker.userEntity.name)} src={selectedMarker.userEntity.avatar} />
                  <span>{selectedMarker.userEntity.name}</span>
                </div>
              </div>

              <div className="budgetBox">
                <p>EARN</p>
                <p>${selectedMarker.budget}</p>
              </div>
            </div>
            <div>
              <div>
                <p>
                  {selectedMarker.postcode}, {selectedMarker.state}
                </p>
                {date}
              </div>
              <Link to={`/browse-tasks/${selectedMarker.id}/${selectedMarker.title}`}>
                <CustomButton variant="contained" size="small">
                  View task
                </CustomButton>
              </Link>
            </div>
          </TaskInfoWindow>
        </InfoWindow>
      ) : null}
    </GoogleMap>
  );
};

Map.propTypes = {
  tasks: PropTypes.array.isRequired,
  address: PropTypes.string.isRequired,
  distance: PropTypes.number.isRequired,
};

export default Map;
