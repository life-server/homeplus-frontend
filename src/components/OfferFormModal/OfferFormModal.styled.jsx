import styled from 'styled-components';

export const Form = styled.form`
  margin: 0 auto;
  padding: 1.5rem;
  display: flex;
  flex-direction: column;
  position: relative;
  button {
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
`;


export const OfferFormBox = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 300px;
  height: 300px;
  border-radius: 10px;
  box-shadow: 24;
  background-color: #fff;
`;