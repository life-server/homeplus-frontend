import React, { useMemo, useState, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import FormControl from '@mui/material/FormControl';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import CustomButton from '../CustomButton';
import useOffer from '../../hooks/useOffer';
import { Form, AcceptRejectOfferBox } from './AcceptRejectOfferModal.style';

const AcceptRejectOfferModal = ({
  status,
  offer,
  setDisplayOffer,
  showModal,
  setShowModal,
  setIsAssigned,
  setTask,
  taskDetails,
}) => {
  const { acceptOffer, rejectOffer, cancelOffer } = useOffer();
  const [decision, setDecision] = useState(status);
  const [acceptRejectInfo, setAcceptRejectInfo] = useState({
    id: offer.id,
    reply_msg: 'I reject your offer',
  });

  const changeOfferStatus = () => {
    switch (decision) {
      case 'ACCEPTED':
        acceptOffer(acceptRejectInfo);
        setIsAssigned(true);
        setTask({ ...taskDetails, task_status: 'assigned' });
        break;
      case 'REJECTED':
        rejectOffer(acceptRejectInfo);
        break;
      case 'CANCELED':
        cancelOffer(acceptRejectInfo);
        setIsAssigned(false);
        setTask({ ...taskDetails, task_status: 'open' });
        break;
      default:
        return;
    }
  };

  useEffect(() => {
    if (decision !== status) {
      setDecision(status);
      setAcceptRejectInfo({ ...acceptRejectInfo, reply_msg: `I ${_.toLower(status)} this offer` });
    }
  }, [decision, status, setDecision, setAcceptRejectInfo, acceptRejectInfo]);

  const title = useMemo(() => {
    return `${_.startCase(_.toLower(decision.substring(0, 6)))} ${offer.taskerEntity.userEntity.name}'s offer`;
  }, [decision, offer]);

  const handleClose = () => setShowModal(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    changeOfferStatus();
    setDisplayOffer({ ...offer, offer_status: status });
    setShowModal(false);
  };

  return (
    <Modal open={showModal} onClose={handleClose}>
      <AcceptRejectOfferBox>
        <h3>{title}</h3>
        <Form onSubmit={handleSubmit}>
          <FormControl sx={{ m: 1 }}>
            <TextField
              id="reply_msg"
              onChange={(e) => setAcceptRejectInfo({ ...acceptRejectInfo, [e.target.name]: e.target.value })}
              value={acceptRejectInfo.reply_msg}
              name="reply_msg"
              label="Message to tasker"
              multiline
              rows={4}
              fullWidth
            />
          </FormControl>
          <CustomButton variant="contained" type="submit">
            send
          </CustomButton>
        </Form>
      </AcceptRejectOfferBox>
    </Modal>
  );
};

AcceptRejectOfferModal.propTypes = {
  offer: PropTypes.object.isRequired,
  status: PropTypes.string.isRequired,
  showModal: PropTypes.bool.isRequired,
  setShowModal: PropTypes.func.isRequired,
  setDisplayOffer: PropTypes.func.isRequired,
  setIsAssigned: PropTypes.func.isRequired,
  setTask: PropTypes.func.isRequired,
  taskDetails: PropTypes.object.isRequired,
};

export default AcceptRejectOfferModal;
