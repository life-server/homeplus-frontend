import styled from 'styled-components';

export const Box = styled.div`
  background-color: #292b32;
  flex-direction: column;
  justify-content: center;
`;
export const Content = styled.div`
  max-width: 1152px;
  margin: 0 auto;
`;
export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  margin: 20px 0;
  @media (max-width: 768px) {
    flex-direction: column;
    padding: 0 20px;
  }
`;
