import styled from 'styled-components';

export const CardContent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: start;
  margin: 0 30px;
  @media (max-width: 1000px) {
    justify-content: center;
  }
`;

export const Logo = styled.img`
  max-width: 30px;
  margin-right: 20px;
  height: auto;
`;
