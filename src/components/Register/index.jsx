import React, { useRef, useEffect, useCallback, useState } from 'react';
import { useSpring, animated } from 'react-spring';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import CustomButton from '../CustomButton';
import SuccessErrorModal from '../SuccessErrorModal';
import useUser from '../../hooks/useUser';

import { Wrapper, Form, Background, ModalWrapper, ModalContent, CloseModalButton, Text } from './Register.style.jsx';

const RegisterModal = ({ showModal, setShowModal, openLoginModal }) => {
  const [openSuccessErrorModal, setSuccessErrorModal] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const [response, setResponse] = useState('');
  const [passwordError, setPasswordError] = useState(false);
  const [passwordFormatError, setPasswordFormatError] = useState(false);
  const [passwardCheck, setPasswordCheck] = useState('');
  const [emailError, setEmailError] = useState(false);
  const [emailCheck, setEmailCheck] = useState('');
  const initData = {
    email: '',
    password: '',
    name: '',
    repassword: '',
  };
  const [userInfo, setUserInfo] = useState(initData);

  const { verifyEmailExists, register } = useUser();
  const modalRef = useRef();

  const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  const passwordRegex = /^(?=.*\d)(?=.*[a-zA-Z]).{8,}$/;

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (passwordError || emailError || passwordFormatError) return;

    const res = await register(userInfo);
    afterSubmit(res);
  };

  const afterSubmit = (res) => {
    setResponse(res);
    const isSsuccess = res === 'success';
    setSuccess(isSsuccess ? true : false);
    if (isSsuccess) {
      setResponse('Check your email inbox to verify registration');
    }
    setUserInfo(initData);
    setShowModal(false);
    setSuccessErrorModal(true);
  };

  const handleEmailChange = async (e) => {
    e.preventDefault();

    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
    const email = e.target.value;
    const result = await verifyEmailExists(email);
    if (!email.match(validRegex)) {
      setEmailCheck('You have entered an invalid email address!');
      setEmailError(true);
    } else if (result.data === 'Email has taken') {
      setEmailCheck('Email has taken');
      setEmailError(true);
    } else {
      setEmailCheck('Email is avaliable');
      setEmailError(false);
    }
  };

  const handleRepasswordChange = (e) => {
    e.preventDefault();
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
    if (userInfo.password && userInfo.password === e.target.value) {
      setPasswordError(false);
      setPasswordCheck('');
    } else {
      setPasswordError(true);
      setPasswordCheck("password doesn't match!");
    }
  };

  const handlePasswordChange = (e) => {
    e.preventDefault();
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
    if (e.target.value.match(passwordRegex)) {
      setPasswordFormatError(false);
    } else {
      setPasswordFormatError(true);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
  };

  const animation = useSpring({
    config: {
      duration: 250,
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`,
  });

  const closeModal = (e) => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    (e) => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(() => {
    document.addEventListener('keydown', keyPress);
    return () => document.removeEventListener('keydown', keyPress);
  }, [keyPress]);
  //TODO: maybe add use google, facebook to register
  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              <ModalContent>
                <Wrapper>
                  <Form onSubmit={handleSubmit}>
                    <TextField
                      type="text"
                      name="name"
                      placeholder="Type your username here..."
                      value={userInfo.name}
                      onChange={handleChange}
                    />
                    <br />
                    <TextField
                      type="email"
                      name="email"
                      placeholder="Type your email here..."
                      value={userInfo.email}
                      onChange={handleEmailChange}
                      helperText={emailCheck}
                      error={emailError}
                    />
                    <br />
                    <TextField
                      type="password"
                      name="password"
                      placeholder="Enter your password..."
                      value={userInfo.password}
                      onChange={handlePasswordChange}
                      error={passwordFormatError}
                      helperText={passwordFormatError ? 'minimum 8 characters long & 1 non-letter character' : null}
                    />
                    <br />
                    <TextField
                      type="password"
                      name="repassword"
                      placeholder="Re-enter your password..."
                      value={userInfo.repassword}
                      onChange={handleRepasswordChange}
                      error={passwordError}
                      helperText={passwordError && passwardCheck}
                    />
                    <br />
                    <CustomButton variant="contained" size="large" type="submit">
                      Register
                    </CustomButton>
                    <br />
                    <Text>
                      Already have an account?
                      <CustomButton color="black" size="small" onClick={openLoginModal}>
                        here
                      </CustomButton>
                    </Text>
                  </Form>
                </Wrapper>
              </ModalContent>
              <CloseModalButton aria-label="Close modal" onClick={() => setShowModal((prev) => !prev)} />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
      <SuccessErrorModal
        isSuccess={isSuccess}
        setShowModal={setSuccessErrorModal}
        showModal={openSuccessErrorModal}
        response={response}
      />
    </>
  );
};

RegisterModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
  openLoginModal: PropTypes.func,
};

export default RegisterModal;
