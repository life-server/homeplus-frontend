import styled from 'styled-components';

export const EditBox = styled.div`
  width: 25px;
  height: 25px;
  background-color: #afafaf;
  color: white;
  border-radius: 50%;
  display: none;
  align-items: center;
  justify-content: center;
  position: absolute;
  bottom: 10px;
  right: 5px;
  @media (max-width: 768px) {
    display: flex;
  }
`;
