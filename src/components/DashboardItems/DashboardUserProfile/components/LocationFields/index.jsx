import { React } from 'react';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const LocationFields = ({ formData, setFormData }) => {
  const stateItems = ['NSW', 'VIC', 'QLD', 'WA', 'SA', 'TAS', 'ACT', 'NT'];

  return (
    <div style={{ display: 'flex', justifyContent: 'space-between', margin: '10px' }}>
      <TextField
        required
        id="suburb"
        label="Suburb"
        variant="outlined"
        name="suburb"
        value={formData.street ? formData.street : ''}
        onChange={(e) => setFormData({ ...formData, street: e.target.value })}
        sx={{ width: 130, margin: '2px' }}
      />
      <FormControl sx={{ width: 100, margin: '2px' }} className="selector">
        <InputLabel id="state-select-label">State</InputLabel>
        <Select
          labelId="state-select-label"
          name="state"
          value={formData.state ? formData.state : ''}
          label="State"
          required
          onChange={(e) => setFormData({ ...formData, state: e.target.value })}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {stateItems.map((item, index) => (
            <MenuItem value={item} key={index}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <TextField
        required
        id="postcode"
        label="Postcode"
        variant="outlined"
        name="postcode"
        value={formData.postcode ? formData.postcode : ''}
        onChange={(e) => setFormData({ ...formData, postcode: e.target.value })}
        sx={{ width: 120, margin: '2px' }}
        type="number"
        error={formData.postcode !== null && formData.postcode.length !== 4}
        helperText={formData.postcode !== null && formData.postcode.length !== 4 ? 'Incorrect' : ' '}
      />
    </div>
  );
};

LocationFields.propTypes = {
  formData: PropTypes.object.isRequired,
  setFormData: PropTypes.func.isRequired,
};

export default LocationFields;
