import styled from 'styled-components';

export const UserForm = styled.form`
  > div {
    margin: 10px;
  }
`;
