import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import format from 'date-fns/format';
import InputSet from '../InputSet';
import SelectionSet from '../SelectionSet';
import DatePicker from '../../../../FormItems/DatePicker';
import CustomButton from '../../../../CustomButton';
import LocationFields from '../LocationFields';
import { UserForm } from './ProfileForm.style';
import useUser from '../../../../../hooks/useUser';
import { setUser } from '../../../../../store/reducers/user/user.actions';

const ProfileForm = ({ setIsEditting }) => {
  const { user } = useSelector((state) => state.currentUser);
  const { updateUser } = useUser();
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    gender: user.gender,
    date_of_birth: user.date_of_birth,
    street: user.street,
    postcode: user.postcode,
    state: user.state,
    mobile: user.mobile,
  });

  const handleDateChange = (value) => {
    setFormData({ ...formData, date_of_birth: format(value, "yyyy-MM-dd'T'HH:mm:ss.SSSxxx") });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      dispatch(setUser({ ...user, ...formData }));
      updateUser({ ...user, ...formData });
      setIsEditting(false);
    } catch (error) {}
  };

  return (
    <UserForm onSubmit={handleSubmit}>
      <SelectionSet formData={formData} setFormData={setFormData} />
      <InputSet title="Mobile" name="mobile" formData={formData} setFormData={setFormData} />
      <DatePicker
        label="Date of Birth"
        openTo="year"
        value={formData.date_of_birth}
        onChange={(value) => handleDateChange(value)}
        className="datePicker"
        disableFuture
      />

      <LocationFields formData={formData} setFormData={setFormData} />

      <div
        className="submit"
        style={{ width: '92%', display: 'flex', alignItems: 'center', justifyContent: 'end', margin: '1rem' }}
      >
        <CustomButton color="primary" variant="contained" type="submit">
          Save
        </CustomButton>
      </div>
    </UserForm>
  );
};

ProfileForm.propTypes = {
  setIsEditting: PropTypes.func.isRequired,
};

export default ProfileForm;
