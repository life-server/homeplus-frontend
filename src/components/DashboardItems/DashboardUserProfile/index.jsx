import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Wrapper, Left, PicArea, Username, LineWrapper, Labal, Value } from './DashboardUserProfile.style';
import AvatarUploader from '../../AvatarUploader';
import ProfileForm from './components/ProfileForm';

const DashboardUserProfile = ({ isEditting, setIsEditting }) => {
  const { user } = useSelector((state) => state.currentUser);

  const address = useMemo(() => {
    if (user.postcode) {
      return `${user.street}, ${user.state}, ${user.postcode}`;
    } else {
      return 'NaN';
    }
  }, [user]);

  return (
    <>
      <Wrapper>
        <Left>
          <PicArea>
            <AvatarUploader />
            <Username>{user.name}</Username>
          </PicArea>
          {!isEditting ? (
            <div className="infoBox">
              <LineWrapper>
                <Labal>Email</Labal>
                <Value>{user.email}</Value>
              </LineWrapper>
              <LineWrapper>
                <Labal>Gender</Labal>
                <Value>{user.gender ? user.gender : 'NaN'}</Value>
              </LineWrapper>
              <LineWrapper>
                <Labal>Mobile</Labal>
                <Value>{user.mobile ? user.mobile : 'NaN'}</Value>
              </LineWrapper>
              <LineWrapper>
                <Labal>DOB</Labal>
                <Value>{user.date_of_birth ? user.date_of_birth.split('T')[0] : 'NaN'}</Value>
              </LineWrapper>

              <LineWrapper>
                <Labal>Address</Labal>
                <Value>{address}</Value>
              </LineWrapper>
            </div>
          ) : (
            <ProfileForm setIsEditting={setIsEditting} />
          )}
        </Left>
      </Wrapper>
    </>
  );
};

DashboardUserProfile.propTypes = {
  isEditting: PropTypes.bool.isRequired,
  setIsEditting: PropTypes.func.isRequired,
};

export default DashboardUserProfile;
