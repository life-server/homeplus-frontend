import styled from 'styled-components';

export const ListItemText = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ul {
    margin-block-end: 0;
  }
  span {
    font-size: 1.5rem;
  }
  @media (max-width: 768px) {
    span {
      font-size: 1.3rem;
      width: 100%;
      // text-align: center;
    }
    button {
      position: absolute;
      right: 15px;
    }
  }
`;
