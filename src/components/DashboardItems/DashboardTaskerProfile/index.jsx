import React from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { Wrapper, LineWrapper, Label, TextArea } from './DashboardTaskerProfile.style';
import Tag from '../../TaskStatusTag';

const TaskerDashboardProfile = () => {
  const { tasker } = useSelector((state) => state.currentUser);
  return (
    <Wrapper>
      <LineWrapper>
        <Label>Job Title</Label>
      </LineWrapper>
      <TextArea>{_.upperFirst(tasker.title)}</TextArea>
      <LineWrapper>
        <Label>Skill Description</Label>
      </LineWrapper>
      <TextArea>{tasker.skills_description}</TextArea>
      <LineWrapper>
        <Label>Category</Label>
      </LineWrapper>
      <TextArea>
        <Tag label={tasker.category} />
      </TextArea>
      <LineWrapper>
        <Label>Certificattions</Label>
      </LineWrapper>
      <TextArea>{tasker.certifications}</TextArea>
      <LineWrapper>
        <Label>Bank Account</Label>
      </LineWrapper>
      <LineWrapper>
        <TextArea>{`${tasker.bank_bsb} - ${tasker.bank_account}`}</TextArea>
      </LineWrapper>
    </Wrapper>
  );
};

export default TaskerDashboardProfile;
