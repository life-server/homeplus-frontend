import styled from 'styled-components';

export const Card = styled.div`
  width: 75%;
  padding: 3%;
  margin-top: 15px;
  background-color: ${({ isAccepted }) => (isAccepted ? '#08658E' : '#444444')};
  color: #fff;

  position: relative;
  margin-bottom: 1rem;
  border-radius: 10px;
  max-height: 45px;
  overflow: hidden;
  max-height: ${({ isOpen }) => (isOpen ? 'none' : '45px')};
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  hr {
    margin-top: 15px;
  }
  :hover {
    cursor: pointer;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.23), 0 6px 6px rgba(0, 0, 0, 0.23);
  }
  @media (max-width: 768px) {
    padding: 5%;
    width: 95%;
  }
`;

export const TitleRow = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  gap: 10px;
  align-items: center;
  @media (max-width: 768px) {
    display: flex;
    justify-content: space-between;
    align-items: initial;
  }
`;

export const TaskerInfo = styled.div`
  display: flex;
  grid-column-start: 1;
  grid-column-end: 3;
  justify-content: space-between;
  padding-right: 25px;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const TaskerAva = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  > p {
    padding-left: 15px;
  }
`;

export const ButtonSet = styled.div`
  display: flex;
  justify-content: flex-end;
  button {
    margin-top: 0;
  }
`;

export const PosterOnly = styled.div`
  display: flex;
  padding-left: 25px;
  align-items: center;
  justify-content: flex-end;
  > p {
    font-size: 1.5rem;
    margin: 0;
    margin-right: 15px;
  }
  @media (max-width: 768px) {
    flex-direction: column;
    padding-left: 0px;
    > p {
      padding-top: 13px;
      font-size: 1.2rem;
      height: 45px;
    }
  }
`;

export const OfferContent = styled.p`
  padding-left: 5px;
`;
