import styled from 'styled-components';

export const Reviewer = styled.div`
  margin: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const RatingBox = styled.div`
  display: flex;
  align-items: center;
  p {
    padding-left: 15px;
    margin: 0;
  }
`;

export const TitileLine = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
`;
