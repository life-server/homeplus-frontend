import React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Link from '@mui/material/Link';
import { WebLinkItem, Title, MobileLinkItem, TitleIcon } from './ExisitingMembers.style.jsx';

const ExisitingMembers = () => {
  const [isExtant, setIsExtant] = React.useState(true);

  const handleClick = () => {
    setIsExtant(!isExtant);
  };

  return (
    <List>
      <ListItemButton onClick={handleClick} sx={{ justifyContent: 'space-between', padding: '0' }}>
        <Title>Exisiting Members</Title>
        <TitleIcon>{isExtant ? <AddIcon sx={{ color: '#fff' }} /> : <RemoveIcon sx={{ color: '#fff' }} />}</TitleIcon>
      </ListItemButton>

      <Collapse in={!isExtant} timeout="auto" unmountOnExit>
        <MobileLinkItem>
          <Link href="#">Post a task</Link>
          <Link href="#">Browse tasks</Link>
          <Link href="#">Login</Link>
          <Link href="#">Support centre</Link>
          <Link href="#">Merchandise</Link>
        </MobileLinkItem>
      </Collapse>
      <WebLinkItem>
        <Link href="#">Post a task</Link>
        <Link href="#">Browse tasks</Link>
        <Link href="#">Login</Link>
        <Link href="#">Support centre</Link>
        <Link href="#">Merchandise</Link>
      </WebLinkItem>
    </List>
  );
};
export default ExisitingMembers;
