import styled from 'styled-components';

export const TitleIcon = styled.div`
  display: none;
  @media (max-width: 768px) {
    display: flex;
  }
`;
export const MobileLinkItem = styled.div`
  display: flex;
  flex-direction: column;
  a:-webkit-any-link {
    color: #bbc2dc;
    margin-bottom: 10px;
  }
  @media (min-width: 768px) {
    display: none;
  }
`;
export const WebLinkItem = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 12px;
  a:-webkit-any-link {
    color: #bbc2dc;
    margin-bottom: 10px;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;

export const Title = styled.h4`
  color: #fff;
  font-size: 14px;
  @media (max-width: 768px) {
    font-size: 16px;
  }
`;
