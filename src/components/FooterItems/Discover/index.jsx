import React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Link from '@mui/material/Link';
import { WebLinkItem, Title, MobileLinkItem, TitleIcon } from './Discover.style.jsx';

const Discover = () => {
  const [isExtant, setIsExtant] = React.useState(true);

  const handleClick = () => {
    setIsExtant(!isExtant);
  };
  return (
    <List>
      <ListItemButton onClick={handleClick} sx={{ justifyContent: 'space-between', padding: '0' }}>
        <Title>Discover</Title>
        <TitleIcon>{isExtant ? <AddIcon sx={{ color: '#fff' }} /> : <RemoveIcon sx={{ color: '#fff' }} />}</TitleIcon>
      </ListItemButton>

      <Collapse in={!isExtant} timeout="auto" unmountOnExit>
        <MobileLinkItem>
          <Link href="#">How it works</Link>
          <Link href="#">HomePlus for business</Link>
          <Link href="#">Earn money</Link>
          <Link href="#">Search jobs</Link>
          <Link href="#">Cost Guides</Link>
          <Link href="#">Service Guides</Link>
          <Link href="#">New users FAQ</Link>
        </MobileLinkItem>
      </Collapse>
      <WebLinkItem>
        <Link href="#">How it works</Link>
        <Link href="#">HomePlus for business</Link>
        <Link href="#">Earn money</Link>
        <Link href="#">Search jobs</Link>
        <Link href="#">Cost Guides</Link>
        <Link href="#">Service Guides</Link>
        <Link href="#">New users FAQ</Link>
      </WebLinkItem>
    </List>
  );
};
export default Discover;
