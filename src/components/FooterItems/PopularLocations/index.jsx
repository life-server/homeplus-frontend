import React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import Collapse from '@mui/material/Collapse';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import Link from '@mui/material/Link';
import { WebLinkItem, Title, MobileLinkItem, TitleIcon } from './PopularLocations.style.jsx';

const PopularLocations = () => {
  const [isExtant, setIsExtant] = React.useState(true);

  const handleClick = () => {
    setIsExtant(!isExtant);
  };

  return (
    <List>
      <ListItemButton onClick={handleClick} sx={{ justifyContent: 'space-between', padding: '0' }}>
        <Title>Popular Locations</Title>
        <TitleIcon>{isExtant ? <AddIcon sx={{ color: '#fff' }} /> : <RemoveIcon sx={{ color: '#fff' }} />}</TitleIcon>
      </ListItemButton>

      <Collapse in={!isExtant} timeout="auto" unmountOnExit>
        <MobileLinkItem>
          <Link href="#">Sydney</Link>
          <Link href="#">Melbourne</Link>
          <Link href="#">Brisbane</Link>
          <Link href="#">Perth</Link>
          <Link href="#">Adelaide</Link>
          <Link href="#">Newcastle</Link>
          <Link href="#">Canberra</Link>
        </MobileLinkItem>
      </Collapse>
      <WebLinkItem>
        <Link href="#">Sydney</Link>
        <Link href="#">Melbourne</Link>
        <Link href="#">Brisbane</Link>
        <Link href="#">Perth</Link>
        <Link href="#">Adelaide</Link>
        <Link href="#">Newcastle</Link>
        <Link href="#">Canberra</Link>
      </WebLinkItem>
    </List>
  );
};
export default PopularLocations;
