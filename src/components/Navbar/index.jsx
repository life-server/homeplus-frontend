import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import LogoImg from '../../assets/icons/logo.png';
import CustomButton from '../CustomButton';
import LoginModal from '../Login';
import RegisterModal from '../Register';
import { Nav, Logo, Menu, Hamburger, Container, ButtonsSet } from './Navbar.style.jsx';
import stringAvatar from '../../utils/avatar.util';

const Navbar = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const currentUser = useSelector((state) => state.currentUser);
  const [login, setLogin] = useState(false);
  const notifications = 0;
  const [showLoginModal, setShowLoginModal] = useState(false);
  const [showRegisterModal, setShowRegisterModal] = useState(false);

  const openLoginModal = () => {
    setShowLoginModal(!showLoginModal);
    setShowRegisterModal(false);
  };

  const openRegisterModal = () => {
    setShowRegisterModal(!showRegisterModal);
    setShowLoginModal(false);
  };

  useEffect(() => {
    if (login !== currentUser.loggedIn) {
      setLogin(currentUser.loggedIn);
    }
    if (currentUser.askLogin) setShowLoginModal(true);
  }, [login, currentUser]);

  return (
    <>
      <Nav>
        <div>
          <div>
            <Link to="/home">
              <Logo src={LogoImg}></Logo>
            </Link>
          </div>
          <Hamburger onClick={() => setIsOpen(!isOpen)}>
            <span className={`burger ${isOpen && 'isopen'}`} />
          </Hamburger>
          <Menu isOpen={isOpen} onClick={() => setIsOpen(false)}>
            <div>
              <Link to="/post-task">
                <CustomButton color="primary" variant="contained">
                  Post a task
                </CustomButton>
              </Link>
              <Link to="/browse-tasks">
                <CustomButton color="black" variant="text">
                  Browse Tasks
                </CustomButton>
              </Link>
            </div>
            <div>
              {currentUser.loggedIn && notifications ? (
                <Badge badgeContent={notifications} color="error">
                  <Link to="/dashboard">
                    <div className="user">
                      <Avatar {...stringAvatar(currentUser.user.name)} src={currentUser.user.avatar} />
                      <p>{currentUser.user.name}</p>
                    </div>
                  </Link>
                </Badge>
              ) : currentUser.loggedIn ? (
                <Link to="/dashboard">
                  <div className="user">
                    <Avatar {...stringAvatar(currentUser.user.name)} src={currentUser.user.avatar} />
                    <p>{currentUser.user.name}</p>
                  </div>
                </Link>
              ) : (
                <ButtonsSet>
                  <CustomButton color="black" onClick={openLoginModal}>
                    Login
                  </CustomButton>
                  <CustomButton color="black" onClick={openRegisterModal}>
                    Register
                  </CustomButton>
                </ButtonsSet>
              )}
            </div>
          </Menu>
        </div>
      </Nav>
      <Container>
        <LoginModal showModal={showLoginModal} setShowModal={setShowLoginModal} openRegisterModal={openRegisterModal} />
        <RegisterModal
          showModal={showRegisterModal}
          setShowModal={setShowRegisterModal}
          openLoginModal={openLoginModal}
        />
      </Container>
    </>
  );
};

export default Navbar;
