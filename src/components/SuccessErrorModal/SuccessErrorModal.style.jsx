import styled from 'styled-components';

export const SuccessErrorBox = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  height: 300px;
  border-radius: 10px;
  box-shadow: 24;
  background-color: #fff;
  align-items: center;
  font-family: Roboto, sans-serif;
  img {
      margin-top: 1.5rem;
  }
  h2, h4 {
      font-weight: 400;
  }
  h2 {
      margin-bottom: .5rem;
  }
`;