import { React } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';

import { updateField } from '../../../../store/reducers/form/form.actions';

const OthersForm = ({ values, updateFields }) => {
  return (
    <FormControl sx={{ m: 1, minWidth: 600 }}>
      <h4>More Description</h4>
      <TextField
        id="description"
        onChange={(e) => updateFields('description', e.target.value)}
        name="description"
        label="More Description"
        multiline
        rows={4}
        value={values.description}
        fullWidth
      />
    </FormControl>
  );
};

OthersForm.propTypes = {
  values: PropTypes.object.isRequired,
  updateFields: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(OthersForm);
