import { React } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { updateField } from '../../../../../store/reducers/form/form.actions';
import { TextFieldGroup } from '../../../styles/TextFieldGroup.style';
import SelectionValues from '../../../utils/SelectionValues';
import LocationFields from '../LocationFields';

const CleaningSelections = ({ values, first2CateErrors, updateFields }) => {
  const handleChange = (type) => (e) => {
    updateFields(type, e.target.value);
  };
  const houseTypes = SelectionValues.typesSelections[0];
  const menuItems = [1, 2, 3, 4, 5];

  return (
    <div>
      <LocationFields first2CateErrors={first2CateErrors} />
      <h4>House Information</h4>
      <TextFieldGroup>
        <div>
          <FormControl sx={{ m: 1, minWidth: 250 }}>
            <InputLabel id="demo-simple-select-label">House Type</InputLabel>
            <Select
              labelId="houseType"
              name="houseType"
              value={values.house_type}
              label="House Type"
              onChange={handleChange('house_type')}
              error={first2CateErrors && values.house_type === ''}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {houseTypes.map((type, index) => (
                <MenuItem value={type} key={index}>
                  {type}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        <div>
          <FormControl sx={{ m: 1, minWidth: 150 }}>
            <InputLabel id="demo-simple-select-label">Number of Rooms</InputLabel>
            <Select
              labelId="numOfRooms"
              name="numOfRooms"
              value={values.num_of_rooms}
              label="Number of Rooms"
              onChange={handleChange('num_of_rooms')}
            >
              <MenuItem value={0}>
                <em>None</em>
              </MenuItem>
              {menuItems.map((item, index) => (
                <MenuItem value={item} key={index}>
                  {item}
                </MenuItem>
              ))}
              <MenuItem value={6}>More than 5</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div>
          <FormControl sx={{ m: 1, minWidth: 150 }}>
            <InputLabel id="demo-simple-select-label">Number of Bathrooms</InputLabel>
            <Select
              labelId="numOfBath"
              name="numOfBath"
              value={values.num_of_bathrooms}
              label="Number of Bathrooms"
              onChange={handleChange('num_of_bathrooms')}
            >
              <MenuItem value={0}>
                <em>None</em>
              </MenuItem>
              {menuItems.map((item, index) => (
                <MenuItem value={item} key={index}>
                  {item}
                </MenuItem>
              ))}
              <MenuItem value={6}>More than 5</MenuItem>
            </Select>
          </FormControl>
        </div>
      </TextFieldGroup>
    </div>
  );
};

CleaningSelections.propTypes = {
  values: PropTypes.object.isRequired,
  updateFields: PropTypes.func,
  first2CateErrors: PropTypes.bool,
};

const mapDispatchToProps = (dispatch) => ({
  updateFields: (name, value) => dispatch(updateField(name, value)),
});

const mapStateToProps = ({ taskForm }) => ({
  values: taskForm,
});

export default connect(mapStateToProps, mapDispatchToProps)(CleaningSelections);
