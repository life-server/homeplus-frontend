import styled from 'styled-components';

export const TextFieldGroup = styled.div`
  display: flex;
  justify-content: space-between;
  width: 600px;
  .MuiFormControl-root {
    margin: 0;
  }

  @media (max-width: 768px) {
    display: inline-grid;
    .selector {
      margin-bottom: 23px;
    }
    > div {
      margin-bottom: 30px;
    }
  }
`;
