import styled from 'styled-components';

export const Loading = styled.div`
  border: 1px solid #444;
  background-color: #fff;
  height: 94vh;
  position: relative;
  .loading {
    position: relative;
    top: 30%;
    display: flex;
    left: 50%;
  }
`;
